#
# Cookbook Name:: my_flask_server
# Recipe:: my_app_dir
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

# app root directory
directory '/var/www/my_flask_app' do
  action :create
  recursive true
  user 'www-data'
  group 'www-data'
end

# app shared directory
directory '/var/www/my_flask_app/shared' do
  action :create
  user 'www-data'
  group 'www-data'
end

# for virtualenv
directory '/var/www/my_flask_app/shared/.env2' do
  action :create
  user 'www-data'
  group 'www-data'
end

# for uwsgi socket and uwsgi config
directory '/var/www/my_flask_app/shared/.uwsgi' do
  action :create
  user 'www-data'
  group 'www-data'
end

# required by ssh
directory '/var/www/.ssh' do
  action :create
  recursive true
  user 'www-data'
  group 'www-data'
end

# required by pip
directory '/var/www/.cache' do
  action :create
  recursive true
  user 'www-data'
  group 'www-data'
end
