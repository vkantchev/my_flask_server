#
# Cookbook Name:: my_flask_server
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

include_recipe 'apt::default'

include_recipe 'my_flask_server::my_app_dir'

include_recipe 'my_flask_server::my_app_python'

include_recipe 'my_flask_server::my_app_ssh'
include_recipe 'my_flask_server::my_app_deploy'

include_recipe 'my_flask_server::my_app_uwsgi'

include_recipe 'my_flask_server::my_app_nginx'
