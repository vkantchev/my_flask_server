#
# Cookbook Name:: my_flask_server
# Recipe:: my_app_uwsgi
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

# Install the uwsgi package in virtual environment using pip
# See https://supermarket.chef.io/cookbooks/poise-python

# install uwsgi python package
python_package 'uwsgi' do
  virtualenv 'my_flask_app_env'
  user 'www-data'
  group 'www-data'
end

# my_flask_app uWSGI configuration
cookbook_file '/var/www/my_flask_app/shared/.uwsgi/my_flask_app.ini' do
  source 'uwsgi/my_flask_app.ini'
  owner 'root'
  group 'root'
  mode 0644
end

# create my_flask_app uWSGI service
cookbook_file '/etc/init/my_flask_app.conf' do
  source 'uwsgi/my_flask_app.conf'
  owner 'root'
  group 'root'
  mode 0644
end

# start my_flask_app uWSGI service
service 'my_flask_app' do
  provider Chef::Provider::Service::Upstart
  supports status: true, restart: true, reload: true
  action [:enable, :start]
end
