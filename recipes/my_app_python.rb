#
# Cookbook Name:: my_flask_server
# Recipe:: my_python
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

# See https://supermarket.chef.io/cookbooks/poise-python

# install python 2
python_runtime 'python_2' do
  version '2'
end

# create my_flask_app virtual environment
python_virtualenv 'my_flask_app_env' do
  path '/var/www/my_flask_app/shared/.env2'
  python 'python_2'
  user 'www-data'
  group 'www-data'
end
