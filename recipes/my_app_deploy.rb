#
# Cookbook Name:: my_flask_server
# Recipe:: my_app_deploy
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

include_recipe 'git::default'

# checkout app from git repo / master branch
# creates `releases`, and `current` directories
# links `current` to latest revision
deploy_revision '/var/www/my_flask_app' do
  action :deploy

  repo 'ssh://git@bitbucket.org/vkantchev/my_flask_app.git'
  branch 'master'

  user 'www-data'
  group 'www-data'

  ssh_wrapper '/tmp/my_flask_app/ssh/wrap-ssh-4-git.sh'

  purge_before_symlink []
  create_dirs_before_symlink []

  symlinks({})

  migrate false
  symlink_before_migrate({})
end

# install python packages in `my_flask_app_env` via pip
# `my_flask_app_env` is defined in the `my_app_python.rb`
pip_requirements '/var/www/my_flask_app/current/requirements.txt' do
  virtualenv 'my_flask_app_env'
  user 'www-data'
  group 'www-data'
  action :install
end
