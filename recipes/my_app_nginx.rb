#
# Cookbook Name:: my_flask_server
# Recipe:: my_nginx
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

# install nginx
package 'nginx' do
  :upgrade
end

# start nginx service
service 'nginx' do
  supports status: true, restart: true, reload: true
  action [:enable, :start]
end

# Uncomment to use a custom nginx.conf
# cookbook_file '/etc/nginx/nginx.conf' do
#   source 'nginx/nginx.conf'
#   mode 0640
#   owner 'root'
#   group 'root'
#   notifies :restart, 'service[nginx]'
# end

# disable default site
link '/etc/nginx/sites-enabled/default' do
  action :delete
  only_if 'test -L /etc/nginx/sites-enabled/default'
  notifies :restart, 'service[nginx]'
end

# add my_flask_app site
cookbook_file '/etc/nginx/sites-available/my_flask_app' do
  source 'nginx/my_flask_app'
  mode 0640
  owner 'root'
  group 'root'
end

# enable my_flask_app site
link '/etc/nginx/sites-enabled/my_flask_app' do
  to '/etc/nginx/sites-available/my_flask_app'
  notifies :restart, 'service[nginx]'
end
