#
# Cookbook Name:: my_flask_server
# Recipe:: my_app_ssh
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

directory '/tmp/my_flask_app/ssh' do
  recursive true
  user 'www-data'
  group 'www-data'
end

# copy git ssh wrapper
cookbook_file '/tmp/my_flask_app/ssh/wrap-ssh-4-git.sh' do
  source 'wrap-ssh-4-git.sh'
  mode 0755
  user 'www-data'
  group 'www-data'
end

# decrypt the private ssh key
my_flask_app = Chef::EncryptedDataBagItem.load('secrets', 'my_flask_app')

if my_flask_app['deploy_key']
  ruby_block 'decrypt `id_my_flask_app_deploy` private ssh key' do
    block do
      f = ::File.open('/tmp/my_flask_app/ssh/id_my_flask_app_deploy', 'w')
      f.print(my_flask_app['deploy_key'])
      f.close
    end

    not_if do
      ::File.exist?('/tmp/my_flask_app/ssh/id_my_flask_app_deploy')
    end
  end

  # change permissions
  file '/tmp/my_flask_app/ssh/id_my_flask_app_deploy' do
    mode 0600
    user 'www-data'
    group 'www-data'
  end
end
