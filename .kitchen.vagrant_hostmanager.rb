# This requires vagrant-hostmanager plugin.
# For more information see https://github.com/smdahlen/vagrant-hostmanager
#
Vagrant.configure("2") do |config|
  if Vagrant.has_plugin?("vagrant-hostmanager")
    # update /ect/hosts on all running guests
    config.hostmanager.enabled = true

    # do not add offline guests to /etc/hosts
    config.hostmanager.include_offline = false

    # use the private ip address with ip_reslover, see below
    config.hostmanager.ignore_private_ip = false

    # custom IP resolver
    # get each guest's IP address by running `hostname -I` on the guest
    config.hostmanager.ip_resolver = proc do |vm, resolving_vm|
      if hostname = (vm.ssh_info && vm.ssh_info[:host])
        `vagrant ssh -c "hostname -I"`.split()[1]
      end
    end

    # also update host's /ect/hosts
    config.hostmanager.manage_host = true
  end
end
