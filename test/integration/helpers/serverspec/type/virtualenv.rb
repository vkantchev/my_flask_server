##############################################################################
# You can find the original code at:
# <https://github.com/jantman/serverspec-extended-types>
# Licensed under the MIT License
##############################################################################

# Serverspec
module Serverspec
  # Type
  module Type
    # Virtualenv
    class Virtualenv < Base
      # Test whether this appears to be a working venv
      #
      # Tests performed:
      # - venv_path/bin/pip executable by owner?
      # - venv_path/bin/python executable by owner?
      # - venv_path/bin/activate readable by owner?
      # - 'export VIRTUAL_ENV' in venv_path/bin/activate?
      #
      # @example
      #   describe virtualenv('/path/to/venv') do
      #     it { should be_virtualenv }
      #   end
      #
      # @api public
      # @return [Boolean]
      def virtualenv?
        pip_path = ::File.join(@name, 'bin', 'pip')
        python_path = ::File.join(@name, 'bin', 'python')
        act_path = ::File.join(@name, 'bin', 'activate')
        cmd = "grep -q 'export VIRTUAL_ENV' #{act_path}"

        @runner.check_file_is_executable(pip_path, 'owner') &&
          @runner.check_file_is_executable(python_path, 'owner') &&
          @runner.check_file_is_readable(act_path, 'owner') &&
          @runner.run_command(cmd).exit_status.to_i == 0
      end
    end

    # Serverspec Type wrapper method for Serverspec::Type::Virtualenv
    #
    # @example
    #   describe virtualenv('/path/to/venv') do
    #     # tests here
    #   end
    #
    # @param name [String] the absolute path to the virtualenv root
    #
    # @api public
    # @return {Serverspec::Type::Virtualenv}
    def virtualenv(name)
      Virtualenv.new(name)
    end
  end
end

include Serverspec::Type
