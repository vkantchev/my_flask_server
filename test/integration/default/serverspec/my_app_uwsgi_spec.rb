require 'spec_helper'

describe 'my_flask_server::my_app_uwsgi' do
  # Serverspec examples can be found at
  # http://serverspec.org/resource_types.html

  # verify uWSGI package
  describe command('/var/www/my_flask_app/shared/.env2/bin/pip list') do
    its(:stdout) { should contain 'uWSGI' }
    its(:exit_status) { should eq 0 }
  end

  # verify my_flask_app uWSGI serice
  describe service('my_flask_app') do
    it { should be_enabled }
    it { should be_running }
  end
end
