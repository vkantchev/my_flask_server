require 'spec_helper'

describe 'my_flask_server::my_app_nginx' do
  # Serverspec examples can be found at
  # http://serverspec.org/resource_types.html

  # verify nginx package
  describe package('nginx') do
    it { should be_installed }
  end

  # verify nginx serice
  describe service('nginx') do
    it { should be_enabled }
    it { should be_running }
  end
end
