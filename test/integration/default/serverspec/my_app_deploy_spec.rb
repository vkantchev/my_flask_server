require 'spec_helper'

describe 'my_flask_server::my_app_deploy' do
  # Serverspec examples can be found at
  # http://serverspec.org/resource_types.html

  # verify git deployment
  describe file('/var/www/my_flask_app/current') do
    it { should be_symlink }
  end

  describe file('/var/www/my_flask_app/current/app.py') do
    it { should exist }
  end

  # verify requirements.txt has been processed
  # i.e. local python packages like `Flask` have been installed
  describe command('/var/www/my_flask_app/shared/.env2/bin/pip list') do
    its(:stdout) { should contain 'Flask' }
    its(:exit_status) { should eq 0 }
  end
end
