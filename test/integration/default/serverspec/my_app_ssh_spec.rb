require 'spec_helper'

describe 'my_flask_server::my_app_ssh' do
  # Serverspec examples can be found at
  # http://serverspec.org/resource_types.html

  describe file('/tmp/my_flask_app/ssh/wrap-ssh-4-git.sh') do
    it { should be_file }
    it { should be_mode '755' }
    it { should be_owned_by 'www-data' }
    it { should be_grouped_into 'www-data' }
  end

  describe file('/tmp/my_flask_app/ssh/id_my_flask_app_deploy') do
    it { should be_file }
    it { should be_mode '600' }
    it { should be_owned_by 'www-data' }
    it { should be_grouped_into 'www-data' }
  end
end
