require 'spec_helper'

describe 'my_flask_server::my_app_dir' do
  # Serverspec examples can be found at
  # http://serverspec.org/resource_types.html

  # verify virtual environment dir
  describe file('/var/www/my_flask_app/shared/.env2') do
    it { should be_directory }
    it { should be_owned_by 'www-data' }
    it { should be_grouped_into 'www-data' }
  end

  # verify uwsgi dir
  describe file('/var/www/my_flask_app/shared/.uwsgi') do
    it { should be_directory }
    it { should be_owned_by 'www-data' }
    it { should be_grouped_into 'www-data' }
  end

  # verify ssh dir
  describe file('/var/www/.ssh') do
    it { should be_directory }
    it { should be_owned_by 'www-data' }
    it { should be_grouped_into 'www-data' }
  end

  # verify pip cache dir
  describe file('/var/www/.cache') do
    it { should be_directory }
    it { should be_owned_by 'www-data' }
    it { should be_grouped_into 'www-data' }
  end
end
