require 'spec_helper'

describe 'my_flask_server::my_app_python' do
  # Serverspec examples can be found at
  # http://serverspec.org/resource_types.html

  # verify system python
  describe package('python') do
    it { should be_installed }
  end

  # verify system pip
  describe command('which pip') do
    its(:stdout) { should contain '/usr/local/bin/pip' }
  end

  # verify system setuptools
  describe command('which easy_install') do
    its(:stdout) { should contain '/usr/local/bin/easy_install' }
  end

  # verify system virtualenv
  describe command('which virtualenv') do
    its(:stdout) { should contain '/usr/local/bin/virtualenv' }
  end

  # verify my_flask_app virtual environment
  # See `helpers/serverspec/type/virtualenv.rb`
  describe virtualenv('/var/www/my_flask_app/shared/.env2') do
    it { should be_virtualenv }
  end
end
