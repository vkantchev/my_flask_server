#
# Cookbook Name:: my_flask_server
# Spec:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

require 'spec_helper'

describe 'my_flask_server::default' do
  context 'When all attributes are default, on an unspecified platform' do
    let(:chef_run) do
      runner = ChefSpec::ServerRunner.new
      runner.converge(described_recipe)
    end

    # Chef stubs
    before do
      allow(Chef::EncryptedDataBagItem)
        .to receive(:load).with('secrets', 'my_flask_app')
        .and_return(
          'deploy_key' => 'abc123'
        )
    end

    # shell command stubs
    before do
      stub_command('test -L /etc/nginx/sites-enabled/default').and_return(0)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end
  end
end
