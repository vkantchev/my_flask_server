# This requires vagrant-cachier plugin.
# For more information see http://fgrehm.viewdocs.io/vagrant-cachier/
#
Vagrant.configure("2") do |config|
  if Vagrant.has_plugin?("vagrant-cachier")
    config.cache.auto_detect = true
    config.cache.scope = :box
  end

  if Vagrant.has_plugin?("vagrant-omnibus")
    config.omnibus.cache_packages = true
    config.omnibus.chef_version = "12.5.1"
  end

  config.vbguest.auto_update = false
end
